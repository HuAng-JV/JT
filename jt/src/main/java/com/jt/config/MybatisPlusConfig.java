package com.jt.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis plus 配置类
 * @author hy
 * @since 2021/10/14
 */
                //什么是bean Spring 容器管理的对象叫做bean
@Configuration //标识标识这是一个配置类
public class MybatisPlusConfig {
    //最新版
    /**
     * 钩子函数（生命周期函数）
     * MP生命周期方法：item.selectPage -- 自动调用mybatisPlusInterceptor()
     * @Bean作用 将方法的返回值交给Spring容器管理
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MARIADB));
        return interceptor;
    }

}
