package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public List<User> findAll() {
        return userService.findAll();
    }

    /**
     * 需求：根据u/p查询数据库，返回秘钥token
     * URL: /user/login
     * 请求方式: POST
     * 参数： username password
     * 返回值 SYSResult对象
     */
    @PostMapping("/login")
    public SysResult login(@RequestBody User user) {
        String token = userService.findUserByUP(user);
        if (token == null || "".equals(token)) {
            return SysResult.fail();
        }
        return SysResult.success(token);
    }

    /**
     * 实现分页查询
     * @param pageResult
     * @return
     */
    @GetMapping("/list")
    public  SysResult getUserListByPage(PageResult pageResult) {
        pageResult = userService.getUserListByPage(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 需求：根据id修改状态
     * @param id
     * @param status
     * @return
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(@PathVariable("id") Integer id,
                                  @PathVariable("status") Boolean status) {
        userService.updateStatus(id, status);
        return SysResult.success();
    }

    /**
     * 添加用户
     * @param user
     * @return
     */
    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user) {
        userService.addUser(user);
        return SysResult.success();
    }

    /**
     * 根据id查询用户数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public SysResult findUserById(@PathVariable("id") Integer id) {
        User user = userService.findUserById(id);
        return SysResult.success(user);
    }

    /**
     * 更新用户数据
     * @param user (phone,email)
     * @return
     */
    @PutMapping("/updateUser")
    public SysResult updateUser(@RequestBody User user) {
        userService.updateUser(user);
        return SysResult.success();
    }

    /**
     * 根据id删除用户数据
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public SysResult deleteUserById(@PathVariable("id") Integer id) {
        userService.deleteUserById(id);
        return SysResult.success();
    }
}
