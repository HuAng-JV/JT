package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品分类
 * @author hy
 * @since 2021/10/13
 */
@RestController
@CrossOrigin
@RequestMapping("/itemCat")
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;

    /**
     * 需求：查询3级分类数据的信息
     * URL:/findItemCatList/{level}
     * @param level
     * @return
     */
    @GetMapping("/findItemCatList/{level}")
    public SysResult findItemCatList(@PathVariable("level") Integer level) {
        List<ItemCat> itemCatList = itemCatService.findItemCatListByLevel(level);
        return SysResult.success(itemCatList);
    }

    /**
     * 实现商品分类新增操作
     * URL：/itemCat/saveItemCat
     * 参数： this.itemCatForm JSON
     */
    @PostMapping("/saveItemCat")
    public SysResult saveItemCat(@RequestBody ItemCat itemCat) {
        itemCatService.saveItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * 修改商品分类状态
     * URL：/itemCat/status/{id}/{status}
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(@PathVariable("id") Integer id, @PathVariable("status") Boolean status) {
        itemCatService.updateStatus(id, status);
        return SysResult.success();
    }

    /**
     * 需求：实现商品分类删除操作
     * 类型：delete
     * URL：/itemCat/deleteItemCat
     * 参数：id/level
     */
    @DeleteMapping("/deleteItemCat")
    public SysResult deleteItemCat(ItemCat itemCat) {
        itemCatService.deleteItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * 需求：修改商品分类
     * 请求路径: /itemCat/updateItemCat
     * 请求类型: put
     * 请求参数: 表单数据 ItemCat对象
     */
    @PutMapping("/updateItemCat")
    public SysResult updateItemCat(@RequestBody ItemCat itemCat) {
        itemCatService.updateItemCat(itemCat);
        return SysResult.success();
    }
}
