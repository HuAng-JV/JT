package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;

/**
 * 文件上传 控制层
 * @author hy
 * @since 2021/10/15
 */
@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {
    @Autowired
    private FileService fileService;
    /**
     * 需求： 实现文件的上传
     * url："http://localhost:8091/file/upload"
     * 类型：post
     * 参数：file
     * 基础知识：
     *      inputStream outputStream
     * 高级API：
     *      SpringMVC提供的接口：
     *      MultipartFile 作用： 接受前端传递过来的字节信息
     * 需求：接收用户信息，保存到本地磁盘中
     * 控制图片大小
     */
//    @PostMapping("/upload") //demo
    public SysResult upload(MultipartFile file) throws IOException {
        //1、获取文件名
        String filename = file.getOriginalFilename();
        //2、封装文件上传目录
        String fileDir = "D:/workspace/image";
        //3、检查目录是否存在
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //4、封装文件的全路径
        String path = new StringBuffer(fileDir).append("/").append(filename).toString();
        //5、上传文件
        file.transferTo(new File(path));
        return SysResult.success();
    }

    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public SysResult uploadFile(MultipartFile file) {
        ImageVO imageVo = fileService.upload(file);
        if (imageVo == null) {
            return SysResult.fail();
        }
        return SysResult.success(imageVo);
    }

    /**
     * 实现文件删除
     * 需求：删除图片信息
     * 类型：delete
     * url：/file/deleteFile
     * 参数：virtualPath
     */
    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath) {
        fileService.deleteFile(virtualPath);
        return SysResult.success();
    }
}
