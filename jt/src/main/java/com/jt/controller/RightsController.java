package com.jt.controller;

import com.jt.pojo.Rights;
import com.jt.service.RightsService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hy
 * @since 2021/10/9
 */
@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {
    @Autowired
    private RightsService rightsService;

    /**
     * 业务说明：完成菜单列表查询1-2级
     * URL：/rights/getRightsList
     * @return list
     */
    @GetMapping("/getRightsList")
    public SysResult getRightsList() {
        List<Rights> rightsList = rightsService.getRightsList();
        return SysResult.success(rightsList);
    }

}
