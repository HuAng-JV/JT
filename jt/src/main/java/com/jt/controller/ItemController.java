package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.service.ItemService;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author hy
 * @since 2021/10/11
 */
@RestController
@CrossOrigin
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private ItemService itemService;

    /**
     * 查询商品列表
     * 实现商品列表的分页展现
     * 类型：get
     * url:/item/getItemList？query=&pageNum=1&pageSize=10
     * @param pageResult
     * @return
     */
    @GetMapping("/getItemList")
    public SysResult getItemList(PageResult pageResult) {
        pageResult = itemService.getItemList(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 更新商品状态
     * @param item
     * @return
     */
    @PutMapping("/updateItemStatus")
    public SysResult updateItemStatus(@RequestBody Item item) {
        itemService.updateItemStatus(item);
        return SysResult.success();
    }


    /**
     * 需求：实现商品的新增
     * URL：/saveItem
     * 类型：post
     * 参数：{商品基本信息对象、商品详情信息对象}
     */
    @PostMapping("/saveItem")
    public SysResult saveItem(@RequestBody ItemVO itemVO) {
        itemService.saveItem(itemVO);
        return SysResult.success();
    }

    /**
     * 需求：实现商品删除操作 关联删除
     * 请求路径: /item/deleteItemById
     * 请求类型: delete
     * 请求参数: id
     */
    @DeleteMapping("/deleteItemById")
    public SysResult deleteItemById(Integer id) {
        itemService.deleteItemById(id);
        return SysResult.success();
    }

    /**
     * 更新商品信息
     * 请求路径：/updateItem
     * 类型：put
     * 参数：item
     */
    @PutMapping("/updateItem")
    public SysResult updateItem(@RequestBody Item item) {
        itemService.updateItem(item);
        return SysResult.success();
    }
}
