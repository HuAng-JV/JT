package com.jt.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * @author hy
 * @since 2021/10/13
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {
    @Autowired
    private ItemCatMapper itemCatMapper;

    @Override
    public List<ItemCat> findItemCatListByLevel(Integer level) {
        Map<Integer, List<ItemCat>> map = getItemCatMap();
        //1级分类
        if (level == 1) {
            return map.get(0);
        }
        //1-2级分类
        if (level == 2) {
           return getTwoItemCatList(map);
        }
        //1-2-3集分类
        return getThreeItemCatList(map);
    }

    /**
     * 优化：自动生成时间
     * 需求说明：如果用户操作每次都需要自己动手操作时间，则代码相对繁琐
     * 优化手段：
     * 1、新增操作时 创建时间/修改时间需要填充
     * 2、更新操作时 操作修改时间
     * @param itemCat
     */

    @Override
    @Transactional
    public void saveItemCat(ItemCat itemCat) {
//        Date date = new Date();
        itemCat.setStatus(true);
//                .setCreated(date).setUpdated(date);
        itemCatMapper.insert(itemCat);
    }

    @Override
    public void updateStatus(Integer id, Boolean status) {
        itemCatMapper.updateStatusById(id, status);
    }

    @Override
    @Transactional
    public void deleteItemCat(ItemCat itemCat) {
        if (itemCat.getLevel() == 3) {
            itemCatMapper.deleteById(itemCat.getId());
            return;
        }
        if (itemCat.getLevel() == 2) {
            QueryWrapper<ItemCat> itemCatQueryWrapper = new QueryWrapper<>();
            //2级商品分类的id = 3级的parent_id,并作为条件构造器的条件
            itemCatQueryWrapper.eq("parent_id", itemCat.getId());
            //删除3级商品分类
            itemCatMapper.delete(itemCatQueryWrapper);
            //删除2级商品分类
            itemCatMapper.deleteById(itemCat.getId());
            return;
        }

        // 删除1级商品分类
        QueryWrapper<ItemCat> itemCatQueryWrapper = new QueryWrapper<>();
        itemCatQueryWrapper.eq("parent_id", itemCat.getId());
        //通过1级id作为2级的parent_id作为条件查询2级的id集合
        List ids = itemCatMapper.selectObjs(itemCatQueryWrapper);
        // 如果1级商品分类下不存在2级商品分类，直接删除1级即可
        if(ids.size()>0) {
            itemCatQueryWrapper.clear();
            //ids = {1,2,3} 利用 in(1,2,3)删除，效率更好
            itemCatQueryWrapper.in("parent_id", ids);
            itemCatMapper.delete(itemCatQueryWrapper);
            //将所有的1-2级的id，封装到一个集合中 一起删除（ID不会重复）
            ids.add(itemCat.getId());
            itemCatMapper.deleteBatchIds(ids);
        } else {
            itemCatMapper.deleteById(itemCat.getId());
        }
    }

    @Override
    public void updateItemCat(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    /**
     * 封装1-2-3级分类
     * @return
     */
    private List<ItemCat> getThreeItemCatList(Map<Integer, List<ItemCat>> map) {
        //获取封装的1-2级分类
        List<ItemCat> oneItemCatList = getTwoItemCatList(map);
        for (ItemCat oneItemCat : oneItemCatList) {
            //获取2级分类
            List<ItemCat> childrenList = oneItemCat.getChildren();
            //如果2级分类没有的情况下 跳过
            if ( childrenList == null || childrenList.isEmpty() ) {
                continue;
            }
            for (ItemCat twoItemCat : childrenList) {
                //2级分类的id = 3级分类的parentId
                List<ItemCat> threeItemCatList = map.get(twoItemCat.getId());
                twoItemCat.setChildren(threeItemCatList);
            }
        }
        return oneItemCatList;
    }

    /**
     * 1-2分类封装
     * @return
     */
    private List<ItemCat> getTwoItemCatList(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> itemCatOneList = map.get(0);
        //遍历一级分类
        for (ItemCat itemCat : itemCatOneList) {
            //一级分类的id = 二级分类的parentId
            List<ItemCat> itemCatTwoList = map.get(itemCat.getId());
            itemCat.setChildren(itemCatTwoList);
        }
        return itemCatOneList;
    }

    /**
     *弊端： 由于多次循环遍历 查询数据库，导致数据库查询次数太多效率极低
     * 优化策略：
     *      降低用户查询次数
     * 优化手段:
     *      1、思路： 获取所有的数据库记录，之后按照父子级刚洗进行封装
     *      2、数据结构：Map<key,value>
     *          Map<parent_id,list当前父级的子级信息（不做嵌套）>
     *              eg: Map<0,List<itemCat>>
     * @return
     */
    public Map<Integer, List<ItemCat>> getItemCatMap() {
        Map<Integer, List<ItemCat>> map = new HashMap<>();
        List<ItemCat> itemCatList = itemCatMapper.selectList(null);
        for (ItemCat itemCat : itemCatList) {
            if (map.containsKey(itemCat.getParentId())) {
                map.get(itemCat.getParentId()).add(itemCat);
            } else {
                List<ItemCat> childrenList = new ArrayList<>();
                childrenList.add(itemCat);
                map.put(itemCat.getParentId(), childrenList);
            }
        }
        return map;
    }


}
