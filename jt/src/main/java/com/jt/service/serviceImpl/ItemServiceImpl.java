package com.jt.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.service.ItemService;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


/**
 * @author hy
 * @since 2021/10/11
 */
@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;



    @Override
    public PageResult getItemList(PageResult pageResult) {
        //方式二：
        QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        //利用条件构造器进行模糊查询SQL拼接构造
        queryWrapper.like(flag,"title", pageResult.getQuery());
        //编辑MP的分页对象 四个属性（页数/条数/总数/记录）传递参数 = (当前页数,页面条数)
        IPage<Item> page = new Page<Item>(pageResult.getPageNum(),pageResult.getPageSize());
        page = itemMapper.selectPage(page, queryWrapper);
        //获取总数和记录数
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());

//        方式一：
//        Long total = itemMapper.queryTotal();
//        //起始页（注：相乘结合起始页为第二页思考）
//        int start = (pageResult.getPageNum() - 1) * pageResult.getPageSize();
//        int size = pageResult.getPageSize();
//        String query = pageResult.getQuery();
//        List<Item> itemList = itemMapper.getItemList(start, size, query);
//        pageResult.setRows(itemList).setTotal(total);
        return pageResult;
    }

    @Override
    public void updateItemStatus(Item item) {
        itemMapper.updateById(item);
    }

    @Override
    public void saveItem(ItemVO itemVO) {
        Item item = itemVO.getItem();
        item.setStatus(true);
        //item没入库之前 itemDesc拿不到id
        //要求item入库之后，动态返回id！！！
        //MP原则：入库之后动态回显数据
        itemMapper.insert(item);
        //实现itemDesc入库
        itemDescMapper.insert(itemVO.getItemDesc().setId(item.getId()));
    }

    @Override
    public void deleteItemById(Integer id) {
        itemMapper.deleteById(id);
        itemDescMapper.deleteById(id);
    }

    @Override
    public void updateItem(Item item) {
        itemMapper.updateById(item);
    }
}
