package com.jt.service.serviceImpl;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;


import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public String findUserByUP(User user) {
        byte[] bytes = user.getPassword().getBytes();
        String md5pass = DigestUtils.md5DigestAsHex(bytes);
        user.setPassword(md5pass);
        User userDB = userMapper.findUserByUP(user);
        if (userDB == null) {
            return null;
        }
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 分页Sql：
     *  select * from user limit 起始位置，查询条数
     *  第一页：
     *  select * from user limit 0, 10 // 0-9 含头不含尾
     *  第二页：
     *  select * from user limit 10, 10 // 10-19
     * @param pageResult
     * @return
     */
    @Override
    public PageResult getUserListByPage(PageResult pageResult) {
        // 1、总数
        Long total = userMapper.findTotal();
        pageResult.setTotal(total);
        // 2、分页结果
        int start = (pageResult.getPageNum()-1) * pageResult.getPageSize();
        int size = pageResult.getPageSize();
        String query = pageResult.getQuery();
        List<User> userList = userMapper.getUserListByPage(start, size, query);
        return pageResult.setTotal(total).setRows(userList);
    }

    @Override
    public void updateStatus(Integer id, Boolean status) {
        userMapper.updateStatus(id, status);
    }

    @Override
    public void addUser(User user) {
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        Date date = new Date();
        user.setStatus(true).setCreated(date).setUpdated(date);
        userMapper.addUser(user);
    }

    @Override
    public User findUserById(Integer id) {
        return userMapper.findUserById(id);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    /**
     * 事务控制：
     * 解决方案 @Transactional注解
     * 作用；
     *  1、默认条件下，只拦截运行时异常
     *  2、rollbackFor：指定异常的类型回滚 rollbackFor = RuntimeException.class
     *  3、noRollBackFor：指定异常不回滚 noRollbackFor = RuntimeException.class
     * @param id
     */
    @Transactional
    @Override
    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }


}
