package com.jt.service.serviceImpl;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件上传 业务层
 * @author hy
 * @since 2021/10/15
 */
@Service
public class FileServiceImpl implements FileService {

//    private String localDir = "D:/workspace/image";
    private String localDir = "/usr/local/src/images";
    private String urlPath = "http://image.jt.com";
    /**
     * 考虑的问题：
     *  1、校验图片类型 xx.jpg/xx.png
     *  2、校验是否为恶意程序 xx.exe.jpg
     *  3、将文件分目录存储
     *  4、为了保证图片唯一性，自定义文件名称
     * @param file
     * @return
     */
    @Override
    public ImageVO upload(MultipartFile file) {
        //1、校验图片类型
        String filename = file.getOriginalFilename().toLowerCase();
        if (!filename.matches("^.+\\.(jpg|png|gif)$")) {
            //如果校验不通过，则终止程序
            return null;
        }

        //2、防止恶意程序 判断图片是否有宽度和高度
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            if (bufferedImage.getWidth() == 0 || bufferedImage.getHeight() == 0) {
                return null;
            }
            //3、目录如何划分 yyyy/MM/dd
            String datePath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
            String dirPath = new StringBuffer(localDir).append(datePath).toString();
            File dirFile = new File(dirPath);
            //判断目录是否存在
            if (!dirFile.exists()) {
                //如果目录不存在，创建目录
                dirFile.mkdirs();
            }
            //4、使用UUID实现文件名称
            int index = filename.lastIndexOf(".");
            //eg: .jpg
            String fileType = filename.substring(index);
            String imageName = UUID.randomUUID().toString().replace("-", "");
            String imageFile = new StringBuffer(dirPath).append(imageName).append(fileType).toString();
            //5、上传文件
            file.transferTo(new File(imageFile));

            //6、返回ImageVo数据
            //6、1 虚拟路径只写动态变化的数据 /2021/11/11/uuid.jpg
            String virtualPath = new StringBuffer(datePath).append(imageName).append(fileType).toString();
            //6、2 真实图片名称
            String fileName = new StringBuffer(imageName).append(fileType).toString();
            //6、3 网络地址 http://image.jt.com/2021/11/11/uuid.jpg
            String url = new StringBuffer(urlPath).append(virtualPath).toString();
            System.out.println(url);
            return new ImageVO(virtualPath,url,fileName);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 1、准备全文件路径
     * 2、实现文件删除
     *
     * @param virtualPath
     */
    @Override
    public void deleteFile(String virtualPath) {
        String path = new StringBuffer(localDir).append(virtualPath).toString();
        File file = new File(path);
        if (file.exists()) {
            //如果文件存在，则删除文件
            file.delete();
        }
    }
}
