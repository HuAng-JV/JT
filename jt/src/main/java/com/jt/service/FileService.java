package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author hy
 * @since 2021/10/15
 */
public interface FileService {
    ImageVO upload(MultipartFile file);

    void deleteFile(String virtualPath);
}
