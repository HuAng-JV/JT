package com.jt.service;

import com.jt.pojo.Item;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;

/**
 * @author hy
 * @since 2021/10/11
 */
public interface ItemService {

    PageResult getItemList(PageResult pageResult);

    void updateItemStatus(Item item);

    void saveItem(ItemVO itemVO);

    void deleteItemById(Integer id);

    void updateItem(Item item);
}
