package com.jt.service;

import com.jt.pojo.ItemCat;
import com.jt.vo.SysResult;

import java.util.List;

/**
 * 商品分类 Service接口
 * @author hy
 * @since 2021/10/13
 */
public interface ItemCatService {

    List<ItemCat> findItemCatListByLevel(Integer level);

    void saveItemCat(ItemCat itemCat);

    void updateStatus(Integer id, Boolean status);

    void deleteItemCat(ItemCat itemCat);

    void updateItemCat(ItemCat itemCat);
}
