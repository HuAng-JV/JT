package com.jt.service;

import com.jt.pojo.Rights;

import java.util.List;

/**
 * @author hy
 * @since 2021/10/9
 */
public interface RightsService {

    List<Rights> getRightsList();
}
