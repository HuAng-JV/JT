package com.jt.pojo;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * item表是商品的基本信息 用户一般查询时都是查询的基本信息，
 * 只有点击某个商品时才会查询详情信息 为了提高查询效率 将商品分为 item/itemDesc
 *
 */

@Data
@Accessors(chain = true)
@TableName("item_desc")
public class ItemDesc extends BasePojo{
    @TableId
    private Integer id; //item.id = itemDesc.id 所以此处主键不可以自增
    private String itemDesc;

}
