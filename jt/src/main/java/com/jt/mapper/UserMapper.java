package com.jt.mapper;

import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;

@Mapper
public interface UserMapper {

    List<User> findAll();

    User findUserByUP(User user);

    Long findTotal();

    List<User> getUserListByPage(@Param("start") int start, @Param("size") int size, @Param("query") String query);

    void updateStatus(@Param("id") Integer id, @Param("status") Boolean status);

    void addUser(User user);

    User findUserById(Integer id);

    void updateUser(User user);

    @Delete("delete from user where id = #{id}")
    void deleteUserById(Integer id);
}
