package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.ItemCat;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


/**
 * 商品分类 Mapper接口
 * @author hy
 * @since 2021/10/13
 */
@Mapper
public interface ItemCatMapper extends BaseMapper<ItemCat> {
    @Update("update item_cat set status = #{status} where id = #{id}")
    void updateStatusById(@Param("id") Integer id, @Param("status") Boolean status);
}
