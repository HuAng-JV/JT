package com.jt.mapper;

import com.jt.pojo.Rights;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author hy
 * @since 2021/10/9
 */
@Mapper
public interface RightsMapper {

    List<Rights> getRightsList();
}
