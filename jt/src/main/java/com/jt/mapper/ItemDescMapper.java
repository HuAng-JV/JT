package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.ItemDesc;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hy
 * @since 2021/10/14
 */
@Mapper
public interface ItemDescMapper extends BaseMapper<ItemDesc> {
}
