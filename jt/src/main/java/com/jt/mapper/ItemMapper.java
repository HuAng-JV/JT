package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Item;
import com.jt.vo.PageResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author hy
 * @since 2021/10/11
 */
@Mapper
public interface ItemMapper extends BaseMapper<Item> {

    @Select("select count(1) from item")
    Long queryTotal();

    List<Item> getItemList(@Param("start") int start, @Param("size") int size, @Param("query") String query);
}
